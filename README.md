# Various Things

This is a collection of things, which can be 3D printed.

## Things

* [Odroid H2 Case](Odroid H2 Case/index.md)

## Licence

All 3D models are licensed under CC-BY-SA

This work is licensed under the Creative Commons Attribution-ShareAlike 3.0 Unported License. To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/3.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
