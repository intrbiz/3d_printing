# Odroid H2 Case

This is a simple and compact case for the [Odroid H2](https://www.odroid.co.uk/ODroid-H2).  The case is in two parts and there are two variants of each half.  The bottom can support two 2.5" HDDs or come in a shorter option.  The top half has an option to use the built in buttons or an external power button.

The case is 132mm wide by 117mm deep.  The storage variant is 75mm tall and the non-storage variant is 56mm tall.  All walls are 3mm thick to make the case sturdy and robust.

The case is designed with vents for fan-less cooling, there is currently not space in the case to fit a fan.

The case is assembled around the Odroid H2 and fastened together using four 3mm x 50mm chipboard screws, these four screws secure the board and case together simultaneous.

## Parts
* h2-short-base.stl  - Short form factor base with no support for storage
* h2-storage-base.stl - Storage form factor base with support for 2x 2.5" HDDs
* h2-storage-hdd-mount-a.stl - HDD mounting bracket
* h2-storage-hdd-mount-b.stl - HDD mounting bracket
* h2-top.stl - Top half with external power button
* h2-top-v2.stl - Top half with support for onboard buttons and LEDs
* button-lever.stl - Onboard button actuator
* button.stl - Front buttons which couple to the button actuator

## Using The Onboard Buttons
The second top variant supports using the onboard power and reset buttons.  These can be activated from the front of the case, two buttons interact with levers to push the onboard buttons.

The buttons are assembled onto the levers, the two parts simply push together.  The buttons then should be slotted through the case so the pivot hole of the lever aligns with the holes in the button supports and the button protrudes from the front slightly.  A 25mm 3mm shaft should then be placed through the supports and button lever pivots and secured in place.  A 25mm M3 bolt or a 3mm by 30mm chipboard screw can easily be used for the pivot shaft.

There are 5 slots in the case to make the onboard LEDs visible, these can optionally be filled with clear epoxy resin (or similar) to form light pipes.

## Using A External Power Button
One top variant does not make use of the onboard power button.  Instead an external power button will need to be connected to the GPIO header and mounted on the case.  There is a whole for a standard 5mm tactile switch on the front of the case.  Follow the Odroid instructions of creating an external power button and glue this to the front of the case.  

Depending on the colour the case is printed in the onboard LEDs might be visible through the case.

## Hard Drive Mounting
The two 2.5"" hard drives are mounted on two rails either side, which are then screwed to the bottom section of the case.

The two rails snap on to the sides of the two hard drives, each hard drive has 4 M3 x 5mm cap head screws in the normal side mounting holes, which the rails slide over.

Once the rails are attached to the hard drives, the whole sub-assembly can be aligned over the two studs on the base and secured gently using a 2.5mm x 15mm chipboard screw.

You will need some fairly small SATA data and power cables as there is not much room in the case.  I used some 10cm flexible SATA data cables.  The SATA power cables were custom made, from a stock Molex to SATA power cable cut down to 10cm and re-terminated with the 4 pin 2.54mm pitch JST XT connectors which the Odroid H2 has on board.


